﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;

namespace ReflecitonTutorial
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        TeamMember t = new TeamMember();
        private void button1_Click(object sender, EventArgs e)
        {
            

            Type tip = t.GetType();
            
            PropertyInfo prop = tip.GetProperty(listBox1.SelectedItem.ToString());
            prop.SetValue(t, textBox1.Text);
            textBox1.Clear();
            
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
            Type ayristir = typeof(TeamMember);
            foreach(var item in ayristir.GetProperties())
            {
                listBox1.Items.Add(item.Name);
            }

            foreach (var item in ayristir.GetMethods())
            {
                listBox2.Items.Add(item.Name);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Type tip = t.GetType();
            PropertyInfo prop = tip.GetProperty(listBox1.SelectedItem.ToString());
            string deger = prop.GetValue(t).ToString();
            MessageBox.Show(deger);
            textBox1.Clear();
        }

       

        private void button4_Click_1(object sender, EventArgs e)
        {
            Type tip = typeof(TeamMember);
            MethodInfo met = tip.GetMethod(listBox2.SelectedItem.ToString());

            string sonuc=met.Invoke(t, null).ToString();
            MessageBox.Show(sonuc);
        }
    }
}
