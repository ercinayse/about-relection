﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReflecitonTutorial
{
    class TeamMember
    {
        
        public string name { get; set; }
        public string lastName { get; set; }
        private string ID { get; set; }
        public string Team { get; set; }

        public string FullName()
        {
            return string.Format("{0} {1}", name, lastName);
        }
    }
}
